<?php


namespace Gamma\Dogs\ViewModel;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Breeds implements ArgumentInterface
{
    /**
     * @var \Gamma\Dogs\Model\Breeds
     */
    protected $breeds;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(
        \Gamma\Dogs\Model\Breeds $breeds,
        RequestInterface $request
    )
    {
        $this->breeds = $breeds;
        $this->request = $request;
    }

    public function getBreeds() {
        $breedsData = $this->breeds->getBreeds();

        if ($breedsData) {
            try {
                return $breedsData;
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

    public function getBreed()
    {
        $requestedBreed = $this->request->getParam('breed');

        if ($requestedBreed) {
            try {
                return $this->breeds->getBreed($requestedBreed);
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

    public function getBooks(){
        if ($this->getBreed()){
            $requestedBreed = $this->request->getParam('breed');

            if ($requestedBreed){
                try{
                    return $this->breeds->getBooks($requestedBreed);
                }catch (Exception $e){
                    return $requestedBreed;
                }
            }
        }
        return "Not info";
    }
}