<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\BreedsInterface;
use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\Data\BreedInterfaceFactory;
use Gamma\Dogs\Api\Data\BookInterfaceFactory;

class Breeds implements BreedsInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var BreedInterfaceFactory
     */
    protected $breedFactory;
    protected $bookFactory;

    public function __construct(
        ConnectionInterface $connection,
        BreedInterfaceFactory $breedFactory,
        BookInterfaceFactory $bookFactory
    )
    {
        $this->connection = $connection;
        $this->breedFactory = $breedFactory;
        $this->bookFactory = $bookFactory;
    }

    public function getBreeds() {

        $breedsData = $this->connection->get("breeds/list/all");

        return $breedsData;

    }

    public function getBreed(string $name)
    {
        $subBreeds = $this->connection->get("breed/${name}/list");

        $image = $this->connection->get("breed/${name}/images/random");

        $breed = $this->breedFactory->create();

        $breed->setName($name)
            ->setImage($image['message'])
            ->setSubBreeds($subBreeds['message']);

        return $breed;
    }

    public function getBooks(string $breed){
        $booksNews = [];
        $booksData = $this->connection->getBooks($breed);

        $books = $booksData['docs'];
        $num = count($books) < 1 ? 0 : count($books) < 2 ? 1 : count($books) < 3 ? 2 : 3;

        for ($i = 0; $i <= $num; $i++) {
            $book = $this->bookFactory->create();
            $book->setTitle($books[$i]['title']);
            array_push($booksNews, $book);
        }

        return $booksNews;
    }
}