<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BreedInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Breed extends AbstractSimpleObject implements BreedInterface {

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): BreedInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getSubBreeds(): array
    {
        return $this->_get(self::SUBBREEDS);
    }

    public function setSubBreeds(array $subBreeds): BreedInterface
    {
        return $this->setData(self::SUBBREEDS, $subBreeds);
    }

    public function getBooks(): array
    {
        return $this->_get(self::BOOKS);
    }

    public function setBooks(array $books): BreedInterface
    {
        return $this->setData(self::BOOKS, $books);
    }

    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName(string $name): BreedInterface
    {
        return $this->setData(self::NAME, $name);
    }
}