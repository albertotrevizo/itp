<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BookInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Book extends AbstractSimpleObject implements BookInterface
{

    public function getTitle(): string
    {
        return $this->_get(self::TITLE);
    }

    public function setTitle(string $title): BookInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    public function getCover(): string
    {
        return $this->_get(self::COVER);
    }

    public function setCover(string $cover): BookInterface
    {
        return $this->setData(self::COVER, $cover);
    }

    public function getReviews(): array
    {
        return $this->_get(self::REVIEWS);
    }

    public function setReviews(array $reviews): BookInterface
    {
        return $this->setData(self::REVIEWS, $reviews);
    }

    public function getLink(): array
    {
        return $this->_get(self::LINK);
    }

    public function setLink(array $link): BookInterface
    {
        return $this->setData(self::LINK, $link);
    }
}