<?php


namespace Gamma\Dogs\Model;


use Gamma\Dogs\Api\ConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class Connection implements ConnectionInterface
{
    protected $baseUrl;
    protected $baseUrlBooks;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://dog.ceo/api',
        string $baseUrlBooks = 'http://openlibrary.org/search.json?subject=dog&title='
    )
    {
        $this->baseUrl = $baseUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
        $this->baseUrlBooks = $baseUrlBooks;
    }

    public function get(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrl}/{$resourcePath}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }

    public function getBooks(string $resourcePath): array
    {
        $requestPath = "{$this->baseUrlBooks}/{$resourcePath}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }
}