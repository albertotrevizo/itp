<?php


namespace Gamma\Dogs\Api\Data;


interface BreedInterface
{
    const NAME = 'name';
    const IMAGE = 'image';
    const SUBBREEDS = 'subbreeds';
    const BOOKS = 'books';

    public function getName(): string;

    public function setName(string $name): BreedInterface;

    public function getImage(): string;

    public function setImage(string $image): BreedInterface;

    public function getSubBreeds(): array;

    public function setSubBreeds(array $subBreeds): BreedInterface;

    public function getBooks(): array;

    public function setBooks(array $books): BreedInterface;
}