<?php


namespace Gamma\Dogs\Api\Data;


interface BookInterface
{
    const TITLE = 'title';
    const COVER = 'cover';
    const REVIEWS = 'reviews';
    const LINK = 'link';

    public function getTitle(): string;

    public function setTitle(string $title): BookInterface;

    public function getCover(): string;

    public function setCover(string $cover): BookInterface;

    public function getReviews(): array;

    public function setReviews(array $reviews): BookInterface;

    public function getLink(): array;

    public function setLink(array $link): BookInterface;
}