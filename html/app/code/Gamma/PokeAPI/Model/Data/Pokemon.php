<?php


namespace Gamma\PokeAPI\Model\Data;


use Gamma\PokeAPI\Api\Data\PokemonInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Pokemon extends AbstractSimpleObject implements PokemonInterface
{

    public function getType(): string
    {
        return $this->_get(self::TYPE);
    }

    public function setType(string $type): PokemonInterface
    {
        return $this->setData(self::TYPE, $type);
    }

    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName(string $name): PokemonInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function getMoves(): array
    {
        return $this->_get(self::MOVES);
    }

    public function setMoves(array $moves): PokemonInterface
    {
        return $this->setData(self::MOVES, $moves);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): PokemonInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getRegion(): string
    {
        return $this->_get(self::REGION);
    }

    public function setRegion(string $region): PokemonInterface
    {
        return $this->setData(self::REGION, $region);
    }

    public function getFlavorText(): string
    {
        return $this->_get(self::FLAVOR_TEXT);
    }

    public function setFlavorText(string $flavorText): PokemonInterface
    {
        return $this->setData(self::FLAVOR_TEXT, $flavorText);
    }
}