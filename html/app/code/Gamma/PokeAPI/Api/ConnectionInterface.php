<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 23/04/19
 * Time: 01:18 PM
 */

namespace Gamma\PokeAPI\Api;


interface ConnectionInterface
{
    public function get(string $resourcePath): array;

}