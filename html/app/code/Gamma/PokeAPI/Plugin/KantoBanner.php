<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 24/04/19
 * Time: 05:04 PM
 */

namespace Gamma\PokeAPI\Plugin;


use Gamma\PokeAPI\Model\Data\Pokemon;

class KantoBanner
{
    public function beforeSetRegion(Pokemon $subject, string $region)
    {
        if(stripos($region, 'kanto') !== false) {
            $region = 'Happyland';
        }

        return [$region];
    }
}