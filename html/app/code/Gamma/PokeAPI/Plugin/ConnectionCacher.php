<?php
/**
 * Created by PhpStorm.
 * User: chris
 * Date: 24/04/19
 * Time: 05:27 PM
 */

namespace Gamma\PokeAPI\Plugin;


use Gamma\PokeAPI\Model\Connection;

class ConnectionCacher
{
    protected $requestedData = [];

    public function aroundGet(Connection $subject, callable $proceed, string $resourcePath) {
        return $this->requestedData[$resourcePath] ?? $this->cacheResponse($proceed, $resourcePath);
    }

    protected function cacheResponse(callable $proceed, string $resourcePath)
    {
        $response = $proceed($resourcePath);

        $this->requestedData[$resourcePath] = $response;

        return $this->requestedData[$resourcePath];
    }
}