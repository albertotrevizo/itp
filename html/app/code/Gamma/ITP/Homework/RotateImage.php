<?php


namespace Gamma\ITP\Homework;


class RotateImage
{
    /**
     * Computers represent images as a 2D array of pixels.
     * Your task is to create a simple program to rotate a single-channel image clockwise 90 degrees
     *
     * Example input:
     * [
     *   [1,2,3],
     *   [4,5,6],
     *   [7,8,9]
     * ],
     *
     * Example output:
     * [
     *   [7,4,1],
     *   [8,5,2],
     *   [9,6,3]
     * ]
     *
     * @param array $image
     * @return array
     */
    public function rotateClockwise(array $image): array
    {
        return $image;
    }
}