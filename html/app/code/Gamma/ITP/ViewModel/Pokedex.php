<?php


namespace Gamma\ITP\ViewModel;


use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Pokedex implements ArgumentInterface
{
    /**
     * @var \Gamma\PokeAPI\Model\Pokedex
     */
    protected $pokedex;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(
        \Gamma\PokeAPI\Model\Pokedex $pokedex,
        RequestInterface $request
    )
    {
        $this->pokedex = $pokedex;
        $this->request = $request;
    }

    public function getPokemon()
    {
        $requestedPokemon = $this->request->getParam('pokemon');

        if ($requestedPokemon) {
            try {
                return $this->pokedex->getPokemon($requestedPokemon);
            } catch (Exception $e) {
                return null;
            }
        }

        return null;
    }

}